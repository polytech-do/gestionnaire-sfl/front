## 1.0.0 (2021-02-14)

##### New Features

- **#22:** Add Metatags - Add metatags - Change favico - update for maticon ([51c4e3e9]('https://gitlab.com/polytech-do/gestionnaire-sfl/front'/commit/51c4e3e9c6055152b87c9639b1419072ee7931dd))
- **#21:** new features - Add csv export - Change UI media form - Add some missins tooltips - Add copy to clipboard sharedlink - fix lapin lang ([507d5483]('https://gitlab.com/polytech-do/gestionnaire-sfl/front'/commit/507d54837d42a235862ba3763c04b522f57dbc56))
- **#20:** new Feature - Fix Bugs - Splash screen - Delete User - new lang ( lapin ) - Change list card ui ([b005d0dc]('https://gitlab.com/polytech-do/gestionnaire-sfl/front'/commit/b005d0dc9b65f35ad3a2e2223b2c31d7bde83d31))
- **#19:** Fix bugs + small update - Fix bugs - Add Errors interceptor - Add Annimations - Add Prettier - Clear services ([41075094]('https://gitlab.com/polytech-do/gestionnaire-sfl/front'/commit/41075094a71edfb33334595a5dc2bbc88877f5b7))
- **#18:**
  - Attempt: 4 - Change env ([872ba056]('https://gitlab.com/polytech-do/gestionnaire-sfl/front'/commit/872ba056ff095e644433cc6a2a7d0559c50294be))
  - Attempt: 3 - Change env ([aef453e7]('https://gitlab.com/polytech-do/gestionnaire-sfl/front'/commit/aef453e7c2b16e2c36505010fbd551b04ca4d0fb))
  - Attempt: 2 - Deploy to firebase ([db9bb436]('https://gitlab.com/polytech-do/gestionnaire-sfl/front'/commit/db9bb4365ed1e80b42cae038181aba8c3e3168f3))
  - Attempt: 1 - Deploy to firebase ([a3fb8546]('https://gitlab.com/polytech-do/gestionnaire-sfl/front'/commit/a3fb8546274eefa472b8c13b5e54fa0bfd440cfa))
- **#17:** implement oAuth2 Google - Fix: bugs - Fix: sharedlinks - Protect route ([5eaa84f9]('https://gitlab.com/polytech-do/gestionnaire-sfl/front'/commit/5eaa84f904a4aa050232629fa4beefde02918b6f))
- **#16:**
  - Fix UI/UX - Add: lottie page 404 + logApp - Rework gestion style with card + border evrywere - Add shared links + comment WIP ([ded11314]('https://gitlab.com/polytech-do/gestionnaire-sfl/front'/commit/ded11314b024f42a326403f9b23b19360cfa117c))
  - Fix UI/UX - Change: referentials to gestion - Add: Sharedlist view - Add: spotlight for list view - Add: filters for list view - Add: 404 page - Fix: media view - Fix: css ([277ba2e7]('https://gitlab.com/polytech-do/gestionnaire-sfl/front'/commit/277ba2e7b406de54c6786b4512d329bc5effc1d4))
- **#15:** Link the API - Add environement - Finalize apiService and crudAbstrcatService - Fix bugs + lint ([87032a5f]('https://gitlab.com/polytech-do/gestionnaire-sfl/front'/commit/87032a5f3c22f625462a114f3781d63024a80de3))
- **#8:** UI Integration ([726bb47f]('https://gitlab.com/polytech-do/gestionnaire-sfl/front'/commit/726bb47fe1f811f172bffc33a613cefe2d003eb1))
- **#14:** Gitlab CI ([bf7c99b0]('https://gitlab.com/polytech-do/gestionnaire-sfl/front'/commit/bf7c99b0be6428579c7d81cc06613bd35fd835d9))
- **#1:** Create base architecture ([03cf098a]('https://gitlab.com/polytech-do/gestionnaire-sfl/front'/commit/03cf098afd09e04c806a062ec456207fb859c8ad))

##### Bug Fixes

- **#23:** Fix bugs - Fix spotlight propagation - Fix submit form bugs - Add en.json - Hide fr.json ([5d9ee690]('https://gitlab.com/polytech-do/gestionnaire-sfl/front'/commit/5d9ee690a81d45d28f5d14fde87792e70e9d27ec))
