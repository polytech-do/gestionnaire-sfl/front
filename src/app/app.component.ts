import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { SecureGuard } from './shared/guard/secure-guard.guard';
import { i18nEnum } from './shared/models/i18n.enum';
import { I18nService } from './shared/services/i18n.service';

@Component({
  selector: 'sfl-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  constructor(translateService: TranslateService, i18nService: I18nService, public secureGuard: SecureGuard) {
    translateService.setDefaultLang(i18nService.getLang());
    translateService.addLangs([i18nEnum.EN, i18nEnum.LAPIN]);
  }
}
