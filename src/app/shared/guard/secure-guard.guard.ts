import { Injectable, OnDestroy } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { Me } from '../data/models/me.type';
import { CurrentUserService } from '../data/services/current-user.service';

@Injectable({
  providedIn: 'root',
})
export class SecureGuard implements CanActivate, OnDestroy {
  public loader = false;
  private meSubscription$: Subscription;

  constructor(private router: Router, private currentUserService: CurrentUserService) {}

  ngOnDestroy(): void {
    this.meSubscription$.unsubscribe();
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
    this.loader = true;
    return new Promise((resolve, reject) => {
      this.meSubscription$ = this.currentUserService.me$.subscribe((me: Me) => {
        if (me) {
          resolve(true);
          this.loader = false;
        } else {
          resolve(false);
          this.router.navigate(['/', 'login']);
          this.loader = false;
        }
      });
    });
  }
}
