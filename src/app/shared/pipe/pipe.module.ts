import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MediaFilterPipe } from './media-filter.pipe';
import { CapitalizePipe } from './capitalize.pipe';

@NgModule({
  declarations: [MediaFilterPipe, CapitalizePipe],
  imports: [CommonModule],
  exports: [MediaFilterPipe, CapitalizePipe],
})
export class PipeModule {}
