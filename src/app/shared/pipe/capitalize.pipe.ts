import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'capitalize',
})
export class CapitalizePipe implements PipeTransform {
  transform(str: string): string {
    return `${str[0].toUpperCase()}${str.toLocaleLowerCase().slice(1, str.length)}`;
  }
}
