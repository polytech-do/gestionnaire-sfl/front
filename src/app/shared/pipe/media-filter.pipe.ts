import { Pipe, PipeTransform } from '@angular/core';
import { Media } from '../data/models/media.type';
import { MediaFilter } from '../models/media-filter.type';
import { some } from 'lodash-es';
import { Genre } from '../data/models/genre.type';

@Pipe({
  name: 'mediaFilter',
})
export class MediaFilterPipe implements PipeTransform {
  transform(media: Media[], mediaFilterForm: MediaFilter, search: string): Media[] {
    return media.filter(
      (m: Media) =>
        this.isInSearch(m, search) &&
        (!this.hasFilter(mediaFilterForm) || this.hasType(m, mediaFilterForm) || this.hasGenres(m, mediaFilterForm))
    );
  }

  isInSearch(media: Media, search: string): boolean {
    return search && search.length > 2 ? media.title.toLocaleLowerCase().includes(search.toLocaleLowerCase()) : true;
  }

  hasFilter(filterFrom: MediaFilter): boolean {
    return !!(!!filterFrom.types.length || filterFrom.genres.length);
  }

  hasType(media: Media, filterFrom: MediaFilter): boolean {
    if (!media.type) {
      return false;
    }
    return some(filterFrom.types, (e: string) => e === media.type);
  }

  hasGenres(media: Media, filterFrom: MediaFilter): boolean {
    let ret = false;
    if (!media.genres) {
      return ret;
    }
    media.genres.forEach((genre: Genre) => {
      if (some(filterFrom.genres, genre)) {
        ret = true;
      }
    });
    return ret;
  }
}
