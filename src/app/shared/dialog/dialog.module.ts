import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatListModule } from '@angular/material/list';

import { TranslateModule } from '@ngx-translate/core';
import { DialogConfirmComponent } from './dialog-confirm/dialog-confirm.component';
import { DialogSharedListAddComponent } from './shared-list-add-dialog/shared-list-add-dialog.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { PipeModule } from '../pipe/pipe.module';
import { ComponentModule } from '../component/component.module';

@NgModule({
  declarations: [DialogConfirmComponent, DialogSharedListAddComponent],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    MatDialogModule,
    MatInputModule,
    MatListModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatIconModule,
    FlexLayoutModule,
    MatSlideToggleModule,
    TranslateModule,
    MatFormFieldModule,
    MatSelectModule,
    PipeModule,
    ComponentModule,
  ],
  exports: [DialogConfirmComponent, DialogSharedListAddComponent],
  entryComponents: [DialogConfirmComponent],
})
export class DialogModule {}
