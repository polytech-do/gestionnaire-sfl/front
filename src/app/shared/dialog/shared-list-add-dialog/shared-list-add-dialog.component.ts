import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { SharedLink } from '../../data/models/shared-link.type';
import { SharedList } from '../../data/models/shared-list.type';
import { Default } from '../../data/models/shared/default.type';
import { SharedLinkService } from '../../data/services/shared-link.service';
import { SharedListService } from '../../data/services/shared-list.service';

@Component({
  selector: 'sfl-shared-list-add-dialog',
  templateUrl: './shared-list-add-dialog.component.html',
  styleUrls: ['./shared-list-add-dialog.component.scss'],
})
export class DialogSharedListAddComponent implements OnInit {
  sharedLinks: SharedLink[];
  sharedLinkFormGroup: FormGroup;

  constructor(
    private sharedLinkService: SharedLinkService,
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<DialogSharedListAddComponent>
  ) {}

  ngOnInit(): void {
    this.initSharedLinkForm();
    this.sharedLinkService.findAll().subscribe((sharedLink: SharedLink[]) => (this.sharedLinks = sharedLink));
  }

  compare(a: Default, b: Default): boolean {
    return a?._id === b?._id;
  }

  initSharedLinkForm(): void {
    this.sharedLinkFormGroup = this.fb.group({
      sharedLinks: [null, Validators.required],
    });
  }

  submitSharedListForm() {
    if (this.sharedLinkFormGroup.valid) {
      const sharedLink: SharedLink = this.sharedLinkFormGroup.value.sharedLinks;
      this.dialogRef.close(sharedLink);
    }
  }
}
