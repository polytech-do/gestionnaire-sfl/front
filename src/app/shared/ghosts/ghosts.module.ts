import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GhostSwiperCardComponent } from './ghost-swiper-card/ghost-swiper-card.component';
import { GhostAvatarComponent } from './ghost-avatar/ghost-avatar.component';
import { GhostLineComponent } from './ghost-line/ghost-line.component';
import { GhostCardComponent } from './ghost-card/ghost-card.component';

@NgModule({
  declarations: [GhostSwiperCardComponent, GhostAvatarComponent, GhostLineComponent, GhostCardComponent],
  imports: [CommonModule],
  exports: [GhostSwiperCardComponent, GhostAvatarComponent, GhostLineComponent, GhostCardComponent],
})
export class GhostsModule {}
