import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'sfl-ghost-avatar',
  templateUrl: './ghost-avatar.component.html',
  styleUrls: ['./ghost-avatar.component.scss'],
})
export class GhostAvatarComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
