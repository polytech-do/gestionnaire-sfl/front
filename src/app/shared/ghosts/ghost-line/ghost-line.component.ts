import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'sfl-ghost-line',
  templateUrl: './ghost-line.component.html',
  styleUrls: ['./ghost-line.component.scss'],
})
export class GhostLineComponent implements OnInit {
  @Input() count = 1;

  constructor() {}

  ngOnInit(): void {}

  lines(): any[] {
    return new Array(this.count);
  }
}
