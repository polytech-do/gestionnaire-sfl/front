import { trigger, style, transition, animate, stagger, query, state } from '@angular/animations';

export const scalefadeAnimation = trigger('scaleFade', [
  transition(':enter', [
    style({
      opacity: 0,
      transform: 'scale(.9)',
    }),
    animate(
      '250ms cubic-bezier(.5, 0, 0, 1)',
      style({
        opacity: 1,
        transform: 'scale(1)',
      })
    ),
  ]),
  transition(':leave', [
    style({
      opacity: 1,
      transform: 'scale(1)',
    }),
    animate(
      '250ms cubic-bezier(.5, 0, 0, 1)',
      style({
        opacity: 0,
        transform: 'scale(.9)',
      })
    ),
  ]),
]);

export const fadeAnimation = trigger('fade', [
  transition(':enter', [
    query('div, .ng-fade', [
      style({ opacity: 0, transform: 'scale(.8)' }),
      stagger('50ms', [
        animate(
          '250ms cubic-bezier(.5, 0, 0, 1)',
          style({
            opacity: 1,
            transform: 'scale(1)',
          })
        ),
      ]),
    ]),
  ]),
]);

export const fadeOutAnimation = trigger('fadeOut', [
  transition('void => *', [style({ opacity: 0 }), animate('800ms cubic-bezier(.5, 0, 0, 1)', style({ opacity: 1 }))]),
  transition('* => void', [animate('1000ms cubic-bezier(.5, 0, 0, 1)', style({ opacity: 0 }))]),
]);
