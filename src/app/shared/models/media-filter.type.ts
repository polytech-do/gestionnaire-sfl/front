import { Genre } from '../data/models/genre.type';
import { TypeEnum } from '../data/models/type.type';

export interface MediaFilter {
  types: TypeEnum[];
  genres: Genre[];
}
