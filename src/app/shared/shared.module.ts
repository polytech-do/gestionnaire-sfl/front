import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PrivatePageModule } from '../private-pages/private-page.module';
import { PublicPageModule } from '../public-pages/public-page.module';
import { DataModule } from './data/data.module';
import { ComponentModule } from './component/component.module';
import { GhostsModule } from './ghosts/ghosts.module';
import { DirectiveModule } from './directive/directive.module';
import { DialogModule } from './dialog/dialog.module';
import { FormsModule } from '@angular/forms';
import { FormularyModule } from './form/formulary.module';
import { MediaFilterPipe } from './pipe/media-filter.pipe';
import { PipeModule } from './pipe/pipe.module';
import { I18nService } from './services/i18n.service';
import { ServicesModule } from './services/services.module';

@NgModule({
  declarations: [],
  exports: [DataModule, ComponentModule, GhostsModule, DirectiveModule, DialogModule, FormularyModule, PipeModule, ServicesModule],
  imports: [CommonModule],
})
export class SharedModule {}
