import { NgModule } from '@angular/core';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { TranslateModule } from '@ngx-translate/core';
import { errorInterceptorProvider } from './error-interceptor.service';
import { tokenInterceptorServiceProvider } from './token-interceptor.service';

@NgModule({
  imports: [MatSnackBarModule, TranslateModule],
  providers: [tokenInterceptorServiceProvider, errorInterceptorProvider],
})
export class InterceptorModule {}
