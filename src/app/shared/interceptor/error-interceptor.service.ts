/* eslint-disable @typescript-eslint/naming-convention */
import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HTTP_INTERCEPTORS, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LoginService } from '../data/services/login.service';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { CurrentUserService } from '../data/services/current-user.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslateService } from '@ngx-translate/core';
import { ApiService } from '../data/services/shared/api.service';

enum ErrorType {
  BAD_REQUEST = 400,
  UNAUTHORIZED = 401,
  NOT_FOUND = 404,
}

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor(
    private router: Router,
    private currentUserService: CurrentUserService,
    private snackBar: MatSnackBar,
    private translateService: TranslateService,
    private apiService: ApiService
  ) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      tap(
        () => {},
        (err: any) => {
          if (err instanceof HttpErrorResponse) {
            switch (err.status) {
              case ErrorType.BAD_REQUEST:
                this.onBadRequest(err);
                break;
              case ErrorType.NOT_FOUND:
                this.onNotFound(err);
                break;
              case ErrorType.UNAUTHORIZED:
                this.onUnauthorized(err);
                break;
              default:
                return;
            }
          }
        }
      )
    );
  }

  onBadRequest(err: HttpErrorResponse) {
    this.openSnackBar(ErrorType.BAD_REQUEST);
    this.router.navigate(['/', 'not-found']);
  }

  onNotFound(err: HttpErrorResponse) {
    this.openSnackBar(ErrorType.NOT_FOUND);
    this.router.navigate(['/', 'not-found']);
  }

  onUnauthorized(err: HttpErrorResponse) {
    if (err.url !== `${this.apiService.endpoint()}/me`) {
      this.currentUserService.me$.next(null);
      this.openSnackBar(ErrorType.UNAUTHORIZED);
      this.router.navigate(['/', 'login']);
    }
  }

  openSnackBar(errorType: ErrorType) {
    this.snackBar.open(
      this.translateService.instant(`shared.interceptor.errors.${errorType}`),
      this.translateService.instant('shared.actions.close'),
      {
        duration: 10000,
        panelClass: ['warn-snackbar'],
        horizontalPosition: 'center',
        verticalPosition: 'top',
      }
    );
  }
}

export const errorInterceptorProvider = [
  {
    provide: HTTP_INTERCEPTORS,
    useClass: ErrorInterceptor,
    multi: true,
  },
];
