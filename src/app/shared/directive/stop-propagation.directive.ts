import { Directive, HostListener } from '@angular/core';

@Directive({
  selector: '[sfl-stop-propagation]',
})
export class StopPropagationDirective {
  @HostListener('click', ['$event'])
  public onClick(event: Event) {
    event.preventDefault();
    event.stopPropagation();
  }
}
