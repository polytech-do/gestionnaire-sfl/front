import { Directive, ElementRef, OnInit } from '@angular/core';

@Directive({
  selector: '[sfl-focus]',
})
export class FocusDirective implements OnInit {
  constructor(private host: ElementRef) {}

  ngOnInit() {
    (this.host.nativeElement as HTMLElement).focus();
  }
}
