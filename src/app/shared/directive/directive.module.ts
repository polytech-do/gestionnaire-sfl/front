import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StopPropagationDirective } from './stop-propagation.directive';
import { FocusDirective } from './focus.directive';

@NgModule({
  declarations: [StopPropagationDirective, FocusDirective],
  imports: [CommonModule],
  exports: [StopPropagationDirective, FocusDirective],
})
export class DirectiveModule {}
