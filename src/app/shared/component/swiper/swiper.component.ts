import { Component, OnInit } from '@angular/core';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';

@Component({
  selector: 'sfl-swiper',
  templateUrl: './swiper.component.html',
  styleUrls: ['./swiper.component.scss'],
})
export class SwiperComponent implements OnInit {
  config: SwiperConfigInterface = {
    init: true,
    observer: true,
    direction: 'horizontal',
    height: 180,
    width: 300,
    effect: 'slide',
    spaceBetween: 16,
    grabCursor: true,
    keyboard: true,
    mousewheel: false,
    scrollbar: false,
    navigation: false,
  };

  constructor() {}

  ngOnInit() {}
}
