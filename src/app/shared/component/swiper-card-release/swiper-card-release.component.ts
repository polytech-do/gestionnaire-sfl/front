import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { Color } from '../../config/enum-color';
import { Serie } from '../../data/models/serie.type';
import { DefaultMedia } from '../../data/models/shared/default-media.type';

@Component({
  selector: 'sfl-swiper-release-card',
  templateUrl: './swiper-card-release.component.html',
  styleUrls: ['./swiper-card-release.component.scss'],
})
export class SwiperCardRelaseComponent implements OnInit {
  @Input() serie: Serie;
  @Output() seenEpisode = new EventEmitter<Serie>();

  constructor(private router: Router) {}

  get color(): typeof Color {
    return Color;
  }

  ngOnInit() {}

  view(): void {
    this.router.navigate(['/', this.serie.type, 'view', this.serie._id]);
  }
}
