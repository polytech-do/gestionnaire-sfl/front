import { Component, Input } from '@angular/core';
import { DefaultMedia } from '../../data/models/shared/default-media.type';

@Component({
  selector: 'sfl-media-view',
  templateUrl: './media-view.component.html',
  styleUrls: ['./media-view.component.scss'],
})
export class MediaViewComponent {
  @Input() media: DefaultMedia;

  constructor() {}
}
