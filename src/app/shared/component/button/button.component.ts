import { Component, ElementRef } from '@angular/core';

const BUTTON_HOST_ATTRIBUTES = ['sfl-button', 'sfl-stroked-button'];
@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: 'button[sfl-button], a[sfl-button], button[sfl-stroked-button], a[sfl-stroked-button]',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
})
export class ButtonComponent {
  constructor(private elementRef: ElementRef) {
    for (const attr of BUTTON_HOST_ATTRIBUTES) {
      if (this.hasHostAttributes(attr)) {
        this.getHostElement().classList.add(attr);
      }
    }
  }

  getHostElement(): HTMLElement {
    return this.elementRef.nativeElement;
  }

  private hasHostAttributes(...attributes: string[]): boolean {
    return attributes.some((attribute) => this.getHostElement().hasAttribute(attribute));
  }
}
