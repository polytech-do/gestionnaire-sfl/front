import { Component, OnInit } from '@angular/core';

import { AnimationOptions } from 'ngx-lottie';

@Component({
  selector: 'sfl-splash-screen',
  templateUrl: './splash-screen.component.html',
  styleUrls: ['./splash-screen.component.scss'],
})
export class SplashScreenComponent implements OnInit {
  options: AnimationOptions = {
    path: 'assets/lottie/splash-screen.json',
    renderer: 'svg',
    autoplay: true,
    loop: true,
  };

  constructor() {}

  ngOnInit() {}
}
