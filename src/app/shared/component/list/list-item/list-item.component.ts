import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'sfl-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.scss'],
})
export class ListItemComponent implements OnInit {
  @Input() router;

  constructor() {}

  ngOnInit(): void {}
}
