import { Component, OnInit } from '@angular/core';
import { AnimationOptions } from 'ngx-lottie';

@Component({
  selector: 'sfl-not-found-splash-screen',
  templateUrl: './not-found-splash-screen.component.html',
  styleUrls: ['./not-found-splash-screen.component.scss'],
})
export class NotFoundSplashScreenComponent implements OnInit {
  options: AnimationOptions = {
    path: 'assets/lottie/404.json',
    renderer: 'svg',
    autoplay: true,
    loop: true,
  };

  constructor() {}

  ngOnInit(): void {}
}
