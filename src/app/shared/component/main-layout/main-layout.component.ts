import { AfterViewInit, Component, ElementRef, HostListener, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MediaChange, MediaObserver } from '@angular/flex-layout';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Media } from '../../config/enum-media';
import { Me } from '../../data/models/me.type';
import { CurrentUserService } from '../../data/services/current-user.service';
import { LoginService } from '../../data/services/login.service';
import { MeService } from '../../data/services/me.service';
import { ToolsService } from '../shared/tools.service';
import { SideNavItem } from './shared/side-nav-items';

@Component({
  selector: 'sfl-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss'],
})
export class MainLayoutComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild('mainNav', { static: false })
  set navBarRef(navBarRef: ElementRef<HTMLElement>) {
    this.navBar = navBarRef?.nativeElement;
  }
  navBar: HTMLElement;

  me: Me;
  items: SideNavItem[];
  lastScrollTop = 0;

  navigationBarSubscription$: Subscription;
  mediaObserverSubscription$: Subscription;

  constructor(
    private toolsService: ToolsService,
    private mediaObserver: MediaObserver,
    private currentUserService: CurrentUserService,
    private loginService: LoginService
  ) {}

  @HostListener('window:scroll', ['$event'])
  onScroll(event) {
    if (window.innerWidth < Media.md) {
      const st = window.pageYOffset || document.documentElement.scrollTop;
      if (st > this.lastScrollTop) {
        // DOWN
        this.navBar.classList.add('hide-to-bottom');
      } else {
        // UP
        if (this.toolsService.getCurrentNavigationStatus()) {
          this.navBar.classList.remove('hide-to-bottom');
        }
      }
      this.lastScrollTop = st <= 0 ? 0 : st;
    }
  }

  ngAfterViewInit(): void {
    this.navigationBarSubscription$ = this.toolsService.navigationBarStatus$.subscribe((status: boolean) => {
      if (status) {
        this.navBar?.classList.remove('hide-to-bottom');
      } else {
        this.navBar?.classList.add('hide-to-bottom');
      }
    });
  }

  ngOnInit(): void {
    this.currentUserService.me$.subscribe((me: Me) => {
      this.me = me;
    });

    this.mediaObserverSubscription$ = this.mediaObserver
      .asObservable()
      .pipe(
        filter((changes: MediaChange[]) => changes.length > 0),
        map((changes: MediaChange[]) => changes[0])
      )
      .subscribe((mediaChange: MediaChange) => {
        if (this.mediaObserver.isActive('gt-sm')) {
          this.navBar?.classList.remove('hide-to-bottom');
        }
      });

    this.items = [
      {
        icon: 'home',
        label: 'mainLayout.nav.items.home',
        routerLink: ['/', 'home'],
      },
      {
        icon: 'list',
        label: 'mainLayout.nav.items.list',
        routerLink: ['/', 'list'],
      },
      {
        icon: 'links',
        label: 'mainLayout.nav.items.sharedLinks',
        routerLink: ['/', 'sharedLinks'],
      },
      {
        icon: 'extension',
        label: 'mainLayout.nav.items.gestion',
        routerLink: ['/', 'gestion'],
      },
    ];
  }

  ngOnDestroy(): void {
    this.navigationBarSubscription$?.unsubscribe();
  }

  logout(): void {
    this.loginService.logout();
  }
}
