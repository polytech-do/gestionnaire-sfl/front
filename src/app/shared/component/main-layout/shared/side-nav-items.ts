export interface SideNavItem {
  icon?: string;
  label: string;
  routerLink?: string[];
}
