import { Component, ElementRef, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { DefaultMedia } from '../../data/models/shared/default-media.type';

@Component({
  selector: 'sfl-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
})
export class CardComponent implements OnInit {
  @Input() media: DefaultMedia;
  @Input() editable = true;
  @Input() customButtonRef: ElementRef;

  // @Input() isSelectable: boolean;
  // @Input() isSelected: boolean;

  // @Output() selected = new EventEmitter<DefaultMedia>();

  constructor(private router: Router) {}

  ngOnInit(): void {}

  view() {
    this.router.navigate([this.media.type, 'view', this?.media?._id]);
  }

  // selectMedia(): void {
  //   this.selected.next(this.media);
  // }
}
