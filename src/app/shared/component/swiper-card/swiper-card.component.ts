import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Color } from '../../config/enum-color';
import { Serie } from '../../data/models/serie.type';
import { DefaultMedia } from '../../data/models/shared/default-media.type';

@Component({
  selector: 'sfl-swiper-card',
  templateUrl: './swiper-card.component.html',
  styleUrls: ['./swiper-card.component.scss'],
})
export class SwiperCardComponent implements OnInit {
  @Input() defaultMedia: DefaultMedia;

  constructor(private router: Router) {}

  get color(): typeof Color {
    return Color;
  }
  ngOnInit() {}

  view(): void {
    this.router.navigate(['/', this.defaultMedia.type, 'view', this.defaultMedia._id]);
  }
}
