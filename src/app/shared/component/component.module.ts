import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainLayoutComponent } from './main-layout/main-layout.component';
import { RouterModule } from '@angular/router';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MatTooltipModule } from '@angular/material/tooltip';
import { TranslateModule } from '@ngx-translate/core';
import { SwiperComponent } from './swiper/swiper.component';
import { SwiperCardComponent } from './swiper-card/swiper-card.component';
import { SwiperModule } from 'ngx-swiper-wrapper';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BarRatingModule } from 'ngx-bar-rating';
import { MatButtonModule } from '@angular/material/button';
import { ButtonComponent } from './button/button.component';
import { ListComponent } from './list/list.component';
import { ListItemComponent } from './list/list-item/list-item.component';
import { FixedBottomActionsComponent } from './fixed-bottom-actions/fixed-bottom-actions.component';
import { CardComponent } from './card/card.component';
import { MediaViewComponent } from './media-view/media-view.component';
import { MatChipsModule } from '@angular/material/chips';
import { GhostsModule } from '../ghosts/ghosts.module';
import { SpotlightComponent } from './spotlight/spotlight.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { DirectiveModule } from '../directive/directive.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ToolsService } from './shared/tools.service';
import { SplashScreenComponent } from './splash-screen/splash-screen.component';
import { LottieModule } from 'ngx-lottie';
import player from 'lottie-web';
import { NotFoundSplashScreenComponent } from './not-found-splash-screen/not-found-splash-screen.component';
import { SwiperCardRelaseComponent } from './swiper-card-release/swiper-card-release.component';

// eslint-disable-next-line prefer-arrow/prefer-arrow-functions
export function playerFactory() {
  return player;
}
@NgModule({
  declarations: [
    MainLayoutComponent,
    SwiperComponent,
    SwiperCardComponent,
    SwiperCardRelaseComponent,
    ButtonComponent,
    ListComponent,
    ListItemComponent,
    FixedBottomActionsComponent,
    CardComponent,
    MediaViewComponent,
    SpotlightComponent,
    SplashScreenComponent,
    NotFoundSplashScreenComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    MatIconModule,
    MatTooltipModule,
    TranslateModule,
    SwiperModule,
    MatCardModule,
    FlexLayoutModule,
    BarRatingModule,
    MatButtonModule,
    MatChipsModule,
    GhostsModule,
    MatFormFieldModule,
    MatInputModule,
    DirectiveModule,
    FormsModule,
    ReactiveFormsModule,
    LottieModule.forRoot({ player: playerFactory }),
  ],
  exports: [
    SwiperComponent,
    SwiperCardComponent,
    ButtonComponent,
    ListComponent,
    ListItemComponent,
    FixedBottomActionsComponent,
    CardComponent,
    MediaViewComponent,
    SpotlightComponent,
    SplashScreenComponent,
    NotFoundSplashScreenComponent,
    SwiperCardRelaseComponent,
  ],
  providers: [ToolsService],
})
export class ComponentModule {}
