import { Injectable, OnDestroy } from '@angular/core';
import { ActivatedRoute, Event, NavigationEnd, Router } from '@angular/router';
import { BehaviorSubject, Subscription } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ToolsService implements OnDestroy {
  navigationBarStatus$ = new BehaviorSubject(true);

  private routerSubscription$: Subscription;
  constructor(private router: Router) {
    this.routerSubscription$ = this.router.events.subscribe((e: Event) => {
      if (e instanceof NavigationEnd && !this.navigationBarStatus$.getValue()) {
        this.toggleNavigationBar(true);
      }
    });
  }

  toggleNavigationBar(status: boolean): void {
    this.navigationBarStatus$.next(status);
  }

  getCurrentNavigationStatus(): boolean {
    return this.navigationBarStatus$.getValue();
  }

  ngOnDestroy(): void {
    this.routerSubscription$?.unsubscribe();
  }
}
