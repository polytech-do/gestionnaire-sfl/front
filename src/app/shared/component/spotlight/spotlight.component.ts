import { Component, ElementRef, HostListener, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MediaObserver } from '@angular/flex-layout';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Event, Router } from '@angular/router';
import { Subject, Subscription } from 'rxjs';
import { SideNavItem } from '../main-layout/shared/side-nav-items';
import { ToolsService } from '../shared/tools.service';

@Component({
  selector: 'sfl-spotlight',
  templateUrl: './spotlight.component.html',
  styleUrls: ['./spotlight.component.scss'],
})
export class SpotlightComponent implements OnDestroy, OnInit {
  search = new Subject<string>();

  @ViewChild('inputRef')
  set inputRef(inputRef: ElementRef<HTMLInputElement>) {
    this.input = inputRef.nativeElement;
  }
  input: HTMLInputElement;

  @ViewChild('containerRef')
  set containerRef(containerRef: ElementRef<HTMLDivElement>) {
    this.container = containerRef.nativeElement;
  }
  container: HTMLDivElement;

  items: (SideNavItem & { select: boolean })[] = [
    {
      label: 'shared.spotlight.items.createSerie',
      routerLink: ['/', 'serie', 'edit'],
      select: false,
    },
    {
      label: 'shared.spotlight.items.createFilm',
      routerLink: ['/', 'film', 'edit'],
      select: false,
    },
    {
      label: 'shared.spotlight.items.createBook',
      routerLink: ['/', 'book', 'edit'],
      select: false,
    },
  ];
  activeItem: number;

  formGroup: FormGroup;
  formGroupSubscription$: Subscription;

  constructor(
    private elementRef: ElementRef,
    private router: Router,
    private fb: FormBuilder,
    private toolsService: ToolsService,
    private mediaObserver: MediaObserver
  ) {}

  @HostListener('mousedown', ['$event'])
  private onClick(event: any) {
    if (event.target.classList.contains('container')) {
      this.hideSpotlight();
    }
  }

  @HostListener('document:keydown', ['$event'])
  private onEsc(event: KeyboardEvent) {
    if (event.key === ' ' && event.ctrlKey) {
      this.openSpotlight();
    }
    if (event.key === 'Escape') {
      this.hideSpotlight();
    }
    if (event.key === 'ArrowDown') {
      const index = this.items.findIndex((elm) => elm.select);
      this.setActiveItem(index + 1);
    }
    if (event.key === 'ArrowUp') {
      const index = this.items.findIndex((elm) => elm.select);
      this.setActiveItem(index - 1);
    }
    if (event.key === 'Enter') {
      event.preventDefault();
      if (this.isActiveItemValide()) {
        this.router.navigate(this.items[this.activeItem].routerLink);
      }
    }
  }

  ngOnInit(): void {
    this.initForm();
  }

  ngOnDestroy(): void {
    this.formGroupSubscription$?.unsubscribe();
  }

  toggleSpotlight(): void {
    if (this.getHostElement().classList.contains('reveal-visible')) {
      this.hideSpotlight();
    } else {
      this.openSpotlight();
    }
  }

  submit() {
    if (this.formGroup.valid) {
      this.search.next(this.formGroup.value.search);
      this.hideSpotlight();
    }
  }

  private openSpotlight() {
    this.getHostElement().classList.add('reveal-visible');
    this.input.focus();
    if (this.mediaObserver.isActive('lt-md')) {
      this.toolsService.toggleNavigationBar(false);
    }
  }

  private initForm(): void {
    this.formGroup = this.fb.group({
      search: [''],
    });
    this.formGroupSubscription$ = this.formGroup.valueChanges.subscribe(() => {
      this.search.next(this.formGroup.value.search);
    });
  }

  private hideSpotlight(e?): void {
    this.input.blur();
    this.getHostElement().classList.remove('reveal-visible');
    if (this.isActiveItemValide()) {
      this.items[this.activeItem].select = false;
    }
    if (this.mediaObserver.isActive('lt-md')) {
      this.toolsService.toggleNavigationBar(true);
    }
  }

  /**
   * @returns true if the active index corresponds to a valid item
   */
  private isActiveItemValide(): boolean {
    return this.activeItem >= 0;
  }

  private setActiveItem(index: number): void {
    if (this.isActiveItemValide()) {
      this.items[this.activeItem].select = false;
    }
    if (index >= this.items.length) {
      index = 0;
    }
    if (index < -1) {
      index = this.items.length - 1;
    }
    if (index === -1) {
      this.items[0].select = false;
      this.activeItem = null;
    } else {
      this.items[index].select = true;
    }
    this.activeItem = index;
  }

  private getHostElement(): HTMLElement {
    return this.elementRef.nativeElement;
  }
}
