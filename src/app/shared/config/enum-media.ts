export enum Media {
  'sm' = 599,
  'md' = 959,
  'lg' = 1279,
  'xl' = 1919,
}
