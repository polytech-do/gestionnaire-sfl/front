export enum Color {
  'primary' = '222831',
  'secondary' = '393E46',
  'accent1' = '00ADB5',
  'accent2' = 'F4BE2E',
  'white' = 'EEEEEE',
}
