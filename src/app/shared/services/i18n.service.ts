import { Injectable } from '@angular/core';
import { i18nEnum } from '../models/i18n.enum';

const I18N_KEY = 'lang';
@Injectable({
  providedIn: 'root',
})
export class I18nService {
  constructor() {}

  setLang(lang: i18nEnum): void {
    localStorage.setItem(I18N_KEY, lang);
  }

  getLang() {
    return localStorage.getItem(I18N_KEY) || i18nEnum.EN;
  }
}
