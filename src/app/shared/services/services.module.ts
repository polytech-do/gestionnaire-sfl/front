import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { I18nService } from './i18n.service';

@NgModule({
  imports: [CommonModule],
  providers: [I18nService],
})
export class ServicesModule {}
