import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CrudComponent } from './crud/crud.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MediaFormComponent } from './media-form/media-form.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { TranslateModule } from '@ngx-translate/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSelectModule } from '@angular/material/select';
import { ReadRessourceComponent } from './crud/read.component';
import { PipeModule } from '../pipe/pipe.module';
import { BarRatingModule } from 'ngx-bar-rating';
import { DirectiveModule } from '../directive/directive.module';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatTooltipModule } from '@angular/material/tooltip';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [CrudComponent, MediaFormComponent, ReadRessourceComponent],
  exports: [CrudComponent, MediaFormComponent, ReadRessourceComponent],
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    TranslateModule,
    FormsModule,
    FlexLayoutModule,
    MatSlideToggleModule,
    MatSelectModule,
    PipeModule,
    MatIconModule,
    MatButtonModule,
    MatTooltipModule,
    BarRatingModule,
    DirectiveModule,
  ],
})
export class FormularyModule {}
