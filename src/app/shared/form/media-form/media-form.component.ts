import { AfterViewChecked, ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { ControlContainer, FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { Genre } from '../../data/models/genre.type';
import { Default } from '../../data/models/shared/default.type';
import { DefaultMedia } from '../../data/models/shared/default-media.type';
import { Type } from '../../data/models/type.type';
import { GenreService } from '../../data/services/genre.service';
import { AuthorService } from '../../data/services/author.service';
import { Author } from '../../data/models/author.type';

@Component({
  selector: 'sfl-media-form',
  templateUrl: './media-form.component.html',
  styleUrls: ['./media-form.component.scss'],
})
export class MediaFormComponent implements OnInit {
  @Input() parentFormGroup: FormGroup;
  genres: Genre[];
  authors: Author[];

  constructor(private genreService: GenreService, private authorService: AuthorService) {}

  compare(a: Default, b: Default): boolean {
    return a?._id === b?._id;
  }

  ngOnInit(): void {
    this.genreService.findAll().subscribe((genres: Genre[]) => (this.genres = genres));
    this.authorService.findAll().subscribe((authors: Author[]) => (this.authors = authors));
  }

  rateChange(rate: number) {
    this.parentFormGroup.get('rate').setValue(rate);
  }
}
