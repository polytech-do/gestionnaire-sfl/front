import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { Default } from '../../data/models/shared/default.type';
import { CrudAbstractService } from '../../data/services/shared/crud-abstract.service';

@Component({
  selector: 'sfl-crud',
  template: ``,
})
export class ReadRessourceComponent<T extends Default> implements OnInit, OnDestroy {
  ressource: T;
  ghosts = new Array(3);
  protected paramMapSubscription$: Subscription;

  constructor(protected service: CrudAbstractService<T>, protected route: ActivatedRoute, protected router: Router) {}

  ngOnInit(): void {
    this.paramMapSubscription$ = this.route.paramMap.subscribe((paramMap: ParamMap) => {
      const id = paramMap.get('id');
      this.service
        .get(id)
        .pipe(finalize(() => (this.ghosts = [])))
        .subscribe({
          next: (ressource: T) => {
            this.ressource = ressource;
            this.readSuccessful();
          },
          error: (e: HttpErrorResponse) => {
            if (e.status === 404) {
              this.router.navigate(['/', 'not-found']);
            }
          },
        });
    });
  }

  ngOnDestroy(): void {
    this.paramMapSubscription$?.unsubscribe();
  }

  protected readSuccessful() {}
}
