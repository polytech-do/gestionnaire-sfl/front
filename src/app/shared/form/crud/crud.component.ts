import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Observable, of, Subscription, throwError } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { Default } from '../../data/models/shared/default.type';
import { DefaultMedia } from '../../data/models/shared/default-media.type';
import { CrudAbstractService } from '../../data/services/shared/crud-abstract.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'sfl-crud',
  template: ``,
})
export class CrudComponent<T extends Default> implements OnInit, OnDestroy {
  isUpdate: boolean;
  isSubmit = false;
  formGroup: FormGroup;
  protected paramMapSubscription$: Subscription;

  constructor(
    protected service: CrudAbstractService<T>,
    protected route: ActivatedRoute,
    protected fb: FormBuilder,
    protected router: Router
  ) {}

  ngOnInit(): void {
    this.paramMapSubscription$ = this.route.paramMap.subscribe((paramMap: ParamMap) => {
      const id = paramMap.get('id');
      this.isUpdate = !!id;
      if (this.isUpdate) {
        this.service.get(id).subscribe({
          next: (type: T) => {
            this.initForm(type);
          },
          error: (e: HttpErrorResponse) => {
            if (e.status === 404) {
              this.router.navigate(['/', 'not-found']);
            }
          },
        });
      } else {
        this.initForm();
      }
    });
  }

  ngOnDestroy(): void {
    this.paramMapSubscription$?.unsubscribe();
  }

  initForm(type?: T): void {}

  send(): Observable<T | void | never> {
    let savingSubscription: Observable<T | void | never> = throwError('invalid form');
    if (this.formGroup.valid) {
      this.isSubmit = true;
      const item: T = this.formGroup.value;
      savingSubscription = this.isUpdate ? this.service.update(item) : this.service.save(item);
      return savingSubscription.pipe(finalize(() => (this.isSubmit = false)));
    }
    return savingSubscription;
  }

  mediaFormBuilder(media: DefaultMedia, other = {}): FormGroup {
    return this.fb.group(Object.assign(this.defaultMediaForm(media), other));
  }

  compare(a: Default, b: Default): boolean {
    return a?._id === b?._id;
  }

  private defaultMediaForm(media: DefaultMedia): { [key: string]: any } {
    return {
      _id: [media._id],
      title: [media.title, Validators.required],
      description: [media.description],
      image: [media.image],
      rate: [media.rate, [Validators.max(5)]],
      genres: [media.genres],
      type: [media.type],
      authors: [media.authors],
      love: [media.love, Validators.required],
      wish: [media.wish, Validators.required],
      inProgress: [media.inProgress, Validators.required],
    };
  }
}
