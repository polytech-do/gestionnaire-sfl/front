export interface Environment {
  gateway: {
    api: {
      name: string;
      version: string;
    };
    endpoint: string;
  };
  origin: string;
}
