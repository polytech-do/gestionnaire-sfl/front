import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MediaQuerryParams } from '../models/filters/media-querry-params.type';
import { Serie } from '../models/serie.type';
import { CrudAbstractService } from './shared/crud-abstract.service';
import { Observable } from 'rxjs';
import { CrudUtilsService } from './shared/crud-utils.service';
import { WeekDay } from '@angular/common';

export interface SerieQuerryParams extends MediaQuerryParams {
  releaseDays?: WeekDay[];
  hasSeen?: boolean;
}
@Injectable({
  providedIn: 'root',
})
export class SerieService extends CrudAbstractService<Serie> {
  constructor(protected http: HttpClient, private crudUtilService: CrudUtilsService) {
    super(http, 'serie');
  }

  findAllWithParams(filters: SerieQuerryParams): Observable<Serie[]> {
    const params = this.crudUtilService.cleanFilters(filters);
    return this.http.get<Serie[]>(this.endpoint, { params });
  }

  count(): Observable<number> {
    return this.http.get<number>(`${this.endpoint}/count`);
  }
}
