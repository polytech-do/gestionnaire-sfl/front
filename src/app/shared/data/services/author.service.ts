import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Author } from '../models/author.type';
import { CrudAbstractService } from './shared/crud-abstract.service';

@Injectable()
export class AuthorService extends CrudAbstractService<Author> {
  constructor(protected http: HttpClient) {
    super(http, 'author');
  }

  count(): Observable<number> {
    return this.http.get<number>(`${this.endpoint}/count`);
  }
}
