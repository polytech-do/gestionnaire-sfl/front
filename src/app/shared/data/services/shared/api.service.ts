import { Injectable } from '@angular/core';
import { environment as ENV } from '../../../../../environments/environment';
import { Environment } from '../../environment/environment';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  constructor() {}

  endpoint(): string {
    const environment: Environment = ENV;
    if (environment.origin === location.origin) {
      return environment.gateway.endpoint;
    } else {
      return 'badOrigin';
    }
  }
}
