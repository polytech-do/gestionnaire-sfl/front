import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslateService } from '@ngx-translate/core';
import { DialogConfirmComponent } from 'src/app/shared/dialog/dialog-confirm/dialog-confirm.component';
import { mapValues, pickBy, isArray } from 'lodash-es';
import * as moment from 'moment';
import { CrudAction } from '../../models/crud-action.enum';
import { MediaQuerryParams } from '../../models/filters/media-querry-params.type';

const SNACKBAR_DEFAULT_DURATION = 5000;

@Injectable()
export class CrudUtilsService {
  constructor(private dialog: MatDialog, private translateService: TranslateService, private snackbar: MatSnackBar) {}

  openDeleteModal(resource: string) {
    const dialogRef = this.dialog.open(DialogConfirmComponent, {
      width: '350px',
      data: {
        title: resource
          ? this.translateService.instant('shared.dialog.content.title', {
              resource: this.translateService.instant(resource),
            })
          : this.translateService.instant('shared.item'),
        message: resource
          ? this.translateService.instant('shared.dialog.content.message', {
              resource: this.translateService.instant(resource),
            })
          : this.translateService.instant('shared.item'),
      },
    });
    return dialogRef.afterClosed();
  }

  displayCustomSnackbar(resource: string, actionKey?: string): void {
    this.snackbar.open(
      this.translateService.instant('shared.snackbar.message', {
        resource: this.translateService.instant(resource),
        action: this.translateService.instant(actionKey),
      }),
      this.translateService.instant('shared.actions.close'),
      { duration: SNACKBAR_DEFAULT_DURATION }
    );
  }

  displaySnackbar(resource: string, action: CrudAction): void {
    this.displayCustomSnackbar(
      resource ? this.translateService.instant(resource) : this.translateService.instant('shared.item'),
      this.translateService.instant(`shared.snackbar.crud.${action}`)
    );
  }

  cleanFilters(filters: MediaQuerryParams): { [param: string]: string | string[] } {
    const cleanedFilters = mapValues(filters, (value) => (value instanceof moment ? moment(value).toISOString() : value));
    return pickBy(cleanedFilters, (value) => (value || value === false) && !(isArray(value) && value.length === 0 ? value : null));
  }
}
