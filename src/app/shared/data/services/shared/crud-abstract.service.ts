import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { inject } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { mapTo, switchMap, tap } from 'rxjs/operators';
import { Default } from '../../models/shared/default.type';
import { ApiService } from './api.service';
import { CrudAction } from '../../models/crud-action.enum';
import { isEmpty } from 'lodash-es';
import { CrudUtilsService } from './crud-utils.service';

@Injectable({
  providedIn: 'root',
})
export abstract class CrudAbstractService<T extends Default> {
  protected endpoint: string;
  protected translateService: TranslateService;

  protected apiService: ApiService;
  private readonly resource: string;
  private crudUtilsService: CrudUtilsService;

  constructor(protected http: HttpClient, protected apiEndpoint: string) {
    this.apiService = inject(ApiService);
    this.endpoint = `${this.apiService.endpoint()}/${this.apiEndpoint}`;

    this.crudUtilsService = inject(CrudUtilsService);
    this.translateService = inject(TranslateService);
    this.resource = this.getResourceKeyFromEndpoint(this.apiEndpoint);
  }

  delete(item: T): Observable<boolean> {
    return this.crudUtilsService.openDeleteModal(this.resource).pipe(
      switchMap((shouldDelete: boolean) => {
        if (shouldDelete) {
          return this.http.delete<void>(`${this.endpoint}/${item._id}`).pipe(
            tap(() => this.crudUtilsService.displaySnackbar(this.resource, CrudAction.delete)),
            mapTo(true)
          );
        }
      })
    );
  }

  findAll(): Observable<T[]> {
    return this.http.get<T[]>(this.endpoint);
  }

  get(id: string): Observable<T> {
    return this.http.get<T>(`${this.endpoint}/${id}`);
  }

  save(item: T): Observable<T> {
    return this.http
      .post<T>(`${this.endpoint}`, item)
      .pipe(tap(() => this.crudUtilsService.displaySnackbar(this.resource, CrudAction.create)));
  }

  update(item: T): Observable<void> {
    return this.http
      .put<void>(`${this.endpoint}/${item._id}`, item)
      .pipe(tap(() => this.crudUtilsService.displaySnackbar(this.resource, CrudAction.update)));
  }

  private getResourceKeyFromEndpoint(endpoint: string): string {
    const endpoints = endpoint.split('/');
    return !isEmpty(endpoints) ? `shared.resources.${endpoints[endpoints.length - 1]}` : 'shared.item';
  }
}
