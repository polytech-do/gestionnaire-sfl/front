import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SharedLink } from '../models/shared-link.type';
import { SharedList } from '../models/shared-list.type';
import { CrudAbstractService } from './shared/crud-abstract.service';

@Injectable({
  providedIn: 'root',
})
export class SharedListService extends CrudAbstractService<SharedList> {
  constructor(protected http: HttpClient) {
    super(http, 'shared-list');
  }
}
