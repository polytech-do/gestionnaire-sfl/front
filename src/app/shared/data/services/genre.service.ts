import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Genre } from '../models/genre.type';
import { CrudAbstractService } from './shared/crud-abstract.service';

@Injectable({
  providedIn: 'root',
})
export class GenreService extends CrudAbstractService<Genre> {
  constructor(protected http: HttpClient) {
    super(http, 'genre');
  }

  count(): Observable<number> {
    return this.http.get<number>(`${this.endpoint}/count`);
  }
}
