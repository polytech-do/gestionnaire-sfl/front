import { isPlatformBrowser } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Inject, Injectable, InjectionToken, PLATFORM_ID } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { CurrentUserService } from './current-user.service';
import { MeService } from './me.service';
import { ApiService } from './shared/api.service';

const TOKEN_KEY = 'token';
@Injectable()
export class LoginService {
  endpoint: string;
  constructor(
    private meService: MeService,
    private currentUserService: CurrentUserService,
    // eslint-disable-next-line @typescript-eslint/ban-types
    @Inject(PLATFORM_ID) private platformId: InjectionToken<Object>,
    private http: HttpClient,
    private router: Router,
    private apiService: ApiService
  ) {
    this.endpoint = this.apiService.endpoint();
  }

  login(): Observable<void> {
    return new Observable((observer) => {
      window.location.href = `${this.endpoint}/oauth2/login/google`;
      observer.complete();
    });
  }

  setToken(token: string): void {
    if (isPlatformBrowser(this.platformId)) {
      window.localStorage.setItem(TOKEN_KEY, token);
    }
  }

  removeToken(): void {
    window.localStorage.removeItem(TOKEN_KEY);
  }

  getToken(): string {
    if (isPlatformBrowser(this.platformId)) {
      return window.localStorage.getItem(TOKEN_KEY);
    }
  }

  logout(): void {
    if (isPlatformBrowser(this.platformId)) {
      this.meService.logout().subscribe(() => {
        this.removeToken();
        this.currentUserService.me$.next(null);
        this.router.navigate(['/', 'login']);
      });
    }
  }
}
