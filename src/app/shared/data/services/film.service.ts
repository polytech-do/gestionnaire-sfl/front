import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Film } from '../models/film.type';
import { CrudAbstractService } from './shared/crud-abstract.service';
import { mapValues, pickBy, isArray } from 'lodash-es';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { MediaQuerryParams } from '../models/filters/media-querry-params.type';
import { CrudUtilsService } from './shared/crud-utils.service';
@Injectable({
  providedIn: 'root',
})
export class FilmService extends CrudAbstractService<Film> {
  constructor(protected http: HttpClient, private crudUtilService: CrudUtilsService) {
    super(http, 'film');
  }

  findAllWithParams(filters: MediaQuerryParams): Observable<Film[]> {
    const params = this.crudUtilService.cleanFilters(filters);
    return this.http.get<Film[]>(this.endpoint, { params });
  }

  count(): Observable<number> {
    return this.http.get<number>(`${this.endpoint}/count`);
  }
}
