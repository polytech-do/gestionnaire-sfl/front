import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Type } from '../models/type.type';
import { CrudAbstractService } from './shared/crud-abstract.service';

@Injectable({
  providedIn: 'root',
})
export class TypeService extends CrudAbstractService<Type> {
  constructor(protected http: HttpClient) {
    super(http, 'type');
  }
}
