import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Media } from '../models/media.type';
import { ApiService } from './shared/api.service';

@Injectable()
export class MediaService {
  private endpoint: string;
  constructor(private http: HttpClient, private apiService: ApiService) {
    this.endpoint = this.apiService.endpoint();
  }

  findAll(): Observable<Media[]> {
    return this.http.get<Media[]>(`${this.endpoint}/media`);
  }
}
