import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Serie } from '../models/serie.type';
import { User } from '../models/user.type';
import { CrudAbstractService } from './shared/crud-abstract.service';

@Injectable({
  providedIn: 'root',
})
export class UserService extends CrudAbstractService<User> {
  constructor(protected http: HttpClient) {
    super(http, 'user');
  }
}
