import { Injectable } from '@angular/core';
import { ReplaySubject, Subject } from 'rxjs';
import { Me } from '../models/me.type';
import { MeService } from './me.service';

@Injectable()
export class CurrentUserService {
  private _me$: Subject<Me>;

  constructor(private meService: MeService) {}

  get me$(): Subject<Me> {
    if (!this._me$) {
      this._me$ = new ReplaySubject(1);
      this.updateMe();
    }
    return this._me$;
  }

  updateMe(): void {
    this.meService.me().subscribe(
      (me: Me) => this.me$.next(me),
      () => this.me$.next(null)
    );
  }
}
