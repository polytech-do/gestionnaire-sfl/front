import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SharedLink } from '../models/shared-link.type';
import { CrudAbstractService } from './shared/crud-abstract.service';

@Injectable({
  providedIn: 'root',
})
export class SharedLinkService extends CrudAbstractService<SharedLink> {
  constructor(protected http: HttpClient) {
    super(http, 'shared-link');
  }
}
