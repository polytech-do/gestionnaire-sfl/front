import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';
import { Me } from '../models/me.type';
import { ApiService } from './shared/api.service';

@Injectable()
export class MeService {
  private endpoint: string;
  constructor(private http: HttpClient, private apiService: ApiService) {
    this.endpoint = this.apiService.endpoint();
  }

  logout(): Observable<void> {
    return this.http.delete<void>(`${this.endpoint}/me`);
  }

  me(): Observable<Me> {
    return this.http.get<Me>(`${this.endpoint}/me`);
  }
}
