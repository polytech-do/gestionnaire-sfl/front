import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { AuthorService } from './services/author.service';
import { BookService } from './services/book.service';
import { CurrentUserService } from './services/current-user.service';
import { FilmService } from './services/film.service';
import { GenreService } from './services/genre.service';
import { LoginService } from './services/login.service';
import { MeService } from './services/me.service';
import { MediaService } from './services/media.service';
import { SerieService } from './services/serie.service';
import { SharedLinkService } from './services/shared-link.service';
import { SharedListService } from './services/shared-list.service';
import { CrudUtilsService } from './services/shared/crud-utils.service';
import { UserService } from './services/user.service';

@NgModule({
  declarations: [],
  imports: [MatDialogModule, MatSnackBarModule],
  providers: [
    CurrentUserService,
    MeService,
    BookService,
    SerieService,
    FilmService,
    GenreService,
    SharedLinkService,
    SharedListService,
    AuthorService,
    LoginService,
    UserService,
    CrudUtilsService,
    MediaService,
  ],
})
export class DataModule {}
