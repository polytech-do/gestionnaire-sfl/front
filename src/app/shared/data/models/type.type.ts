import { Default } from './shared/default.type';

export enum TypeEnum {
  book = 'book',
  film = 'film',
  serie = 'serie',
}
export interface Type extends Default {
  label: TypeEnum;
}
