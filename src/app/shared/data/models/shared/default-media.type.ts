import { Author } from '../author.type';
import { Genre } from '../genre.type';
import { TypeEnum } from '../type.type';
import { Default } from './default.type';

export interface DefaultMedia extends Default {
  title: string;
  description?: string;
  image?: string;
  rate?: number;
  type: TypeEnum;
  genres?: Genre[];
  authors?: Author[];
  love: boolean;
  wish: boolean;
  inProgress: boolean;
}
