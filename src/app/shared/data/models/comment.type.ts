import { Me } from './me.type';
import { Default } from './shared/default.type';

export interface Comment extends Default {
  text: string;
  author: Me;
}
