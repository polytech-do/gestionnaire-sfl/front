import { Default } from './shared/default.type';
export interface Genre extends Default {
  label: string;
}
