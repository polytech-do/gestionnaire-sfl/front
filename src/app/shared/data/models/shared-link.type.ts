import { Me } from './me.type';
import { SharedList } from './shared-list.type';
import { Default } from './shared/default.type';
export interface SharedLink extends Default {
  owner?: Me;
  sharedList: SharedList;
}
