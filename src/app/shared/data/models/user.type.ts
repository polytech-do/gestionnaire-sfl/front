import { Default } from './shared/default.type';

export interface User extends Default {
  avatar?: string;
  email: string;
  displayName: string;
}
