import { Default } from './shared/default.type';

export interface Author extends Default {
  lastName: string;
  firstname: string;
}
