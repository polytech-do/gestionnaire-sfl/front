export enum CrudAction {
  create = 'create',
  update = 'update',
  delete = 'delete',
}
