import { Default } from './shared/default.type';

export interface Me extends Default {
  avatar?: string;
  email: string;
  displayName: string;
}
