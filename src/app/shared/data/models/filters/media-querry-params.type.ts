import { Me } from '../me.type';

export interface MediaQuerryParams {
  inProgress?: boolean;
  title?: string;
  wish?: boolean;
  love?: boolean;
  owner?: Me;
}
