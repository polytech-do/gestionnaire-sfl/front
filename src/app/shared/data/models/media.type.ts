import { Book } from './book.type';
import { Film } from './film.type';
import { Serie } from './serie.type';

export type Media = Serie & Film & Book;
