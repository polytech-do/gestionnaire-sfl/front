import { WeekDay } from '@angular/common';
import { DefaultMedia } from './shared/default-media.type';
export interface Serie extends DefaultMedia {
  startDate?: string;
  endDate?: string;
  releaseDays?: WeekDay[];
  hasSeen?: boolean;
  lasEpisodeSeen?: number;
}
