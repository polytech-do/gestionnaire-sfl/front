import { Book } from './book.type';
import { Comment } from './comment.type';
import { Film } from './film.type';
import { Me } from './me.type';
import { Serie } from './serie.type';
import { Default } from './shared/default.type';

export interface SharedList extends Default {
  label: string;
  author: Me;
  series?: Serie[];
  films?: Film[];
  books?: Book[];
  comments?: Comment[];
}
