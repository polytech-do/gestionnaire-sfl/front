import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '../shared/shared.module';
import { LoginComponent } from './login/login.component';
import { SharedListComponent } from './shared-list/shared-list.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { RouterModule } from '@angular/router';
import { ComponentModule } from '../shared/component/component.module';
import { FormularyModule } from '../shared/form/formulary.module';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { FlexLayoutModule } from '@angular/flex-layout';
import { RedirectComponent } from './redirect/redirect.component';
import { LottieModule } from 'ngx-lottie';
import player from 'lottie-web';
import { MatTooltipModule } from '@angular/material/tooltip';
import { PublicPageRoutingModule } from './public-page-routing.module';

// eslint-disable-next-line prefer-arrow/prefer-arrow-functions
export function playerFactory() {
  return player;
}
@NgModule({
  declarations: [LoginComponent, SharedListComponent, NotFoundComponent, RedirectComponent],
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    ComponentModule,
    FormularyModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    FlexLayoutModule,
    MatTooltipModule,
    PublicPageRoutingModule,
    LottieModule.forRoot({ player: playerFactory }),
  ],
})
export class PublicPageModule {}
