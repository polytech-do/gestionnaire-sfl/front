import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { finalize, switchMap, tap } from 'rxjs/operators';
import { CrudAction } from 'src/app/shared/data/models/crud-action.enum';
import { Film } from 'src/app/shared/data/models/film.type';
import { Me } from 'src/app/shared/data/models/me.type';
import { Media } from 'src/app/shared/data/models/media.type';
import { SharedList } from 'src/app/shared/data/models/shared-list.type';
import { TypeEnum } from 'src/app/shared/data/models/type.type';
import { CurrentUserService } from 'src/app/shared/data/services/current-user.service';
import { LoginService } from 'src/app/shared/data/services/login.service';
import { SharedListService } from 'src/app/shared/data/services/shared-list.service';
import { CrudUtilsService } from 'src/app/shared/data/services/shared/crud-utils.service';
import { ReadRessourceComponent } from 'src/app/shared/form/crud/read.component';

@Component({
  selector: 'sfl-shared-list',
  templateUrl: './shared-list.component.html',
  styleUrls: ['./shared-list.component.scss'],
})
export class SharedListComponent extends ReadRessourceComponent<SharedList> implements OnInit {
  commentformGroup: FormGroup;

  comments: string[] = [];
  commentFocus = false;
  me: Me;

  list: Media[];
  constructor(
    protected sharedListService: SharedListService,
    protected route: ActivatedRoute,
    protected router: Router,
    private fb: FormBuilder,
    private currentUserService: CurrentUserService,
    private crudUtilsService: CrudUtilsService,
    private loginService: LoginService
  ) {
    super(sharedListService, route, router);
  }

  readSuccessful() {
    this.list = [...(this.ressource?.series || []), ...(this.ressource?.films || []), ...(this.ressource?.books || [])];
  }

  delete(item: Media) {
    this.crudUtilsService
      .openDeleteModal(`shared.resources.${item.type}`)
      .pipe(
        switchMap((isDeleted: boolean) => {
          if (isDeleted) {
            switch (item.type) {
              case TypeEnum.serie:
                this.ressource.series = this.ressource.series.filter((s: Film) => s._id !== item._id);
                break;
              case TypeEnum.film:
                this.ressource.films = this.ressource.films.filter((f: Film) => f._id !== item._id);
                break;
              case TypeEnum.book:
                this.ressource.books = this.ressource.books.filter((b: Film) => b._id !== item._id);
                break;
            }
            return this.sharedListService.update(this.ressource).pipe(
              finalize(() => {
                this.crudUtilsService.displaySnackbar(item.type, CrudAction.delete);
              })
            );
          }
        })
      )
      .subscribe(() => {
        this.readSuccessful();
      });
  }

  ngOnInit() {
    super.ngOnInit();

    this.currentUserService.me$.subscribe((me: Me) => {
      this.me = me;
    });

    this.commentformGroup = this.fb.group({
      comment: ['', Validators.required],
    });
  }

  submit() {
    if (this.commentformGroup.valid) {
      this.comments.push(this.commentformGroup.value.comment);
    }
  }

  logout(): void {
    this.loginService.logout();
  }
}
