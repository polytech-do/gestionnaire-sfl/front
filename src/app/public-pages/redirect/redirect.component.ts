import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { AnimationOptions } from 'ngx-lottie';
import { LoginService } from 'src/app/shared/data/services/login.service';

@Component({
  selector: 'sfl-redirect',
  templateUrl: './redirect.component.html',
  styleUrls: ['./redirect.component.scss'],
})
export class RedirectComponent implements OnInit {
  options: AnimationOptions = {
    path: 'assets/lottie/redirection.json',
    renderer: 'svg',
    autoplay: true,
    loop: true,
  };

  constructor(private route: ActivatedRoute, private router: Router, private loginService: LoginService) {}

  ngOnInit(): void {
    this.route.queryParams.subscribe((params: Params) => {
      const token = params.token;
      if (token) {
        this.loginService.setToken(token);
        this.router.navigate(['/', 'home']);
      } else {
        this.router.navigate(['/', 'login']);
      }
    });
  }
}
