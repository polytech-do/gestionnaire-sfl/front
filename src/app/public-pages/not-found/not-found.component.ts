import { Component, ElementRef, OnDestroy, ViewChild } from '@angular/core';

@Component({
  selector: 'sfl-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss'],
})
export class NotFoundComponent implements OnDestroy {
  @ViewChild('videoRef', { static: false })
  set videoRef(videoRef: ElementRef<HTMLVideoElement>) {
    this.video = videoRef.nativeElement;
  }
  video: HTMLVideoElement;

  @ViewChild('rickrollRef', { static: false })
  set rickRollRef(rickRollRef: ElementRef<HTMLDivElement>) {
    this.rickRoll = rickRollRef.nativeElement;
  }
  rickRoll: HTMLDivElement;

  constructor() {}

  ngOnDestroy(): void {
    this.video.parentNode.removeChild(this.video);
    this.rickRoll.parentNode.removeChild(this.rickRoll);
  }

  onClick() {
    this.rickRoll.classList.add('reveal-visible');
    this.video.play();
  }
}
