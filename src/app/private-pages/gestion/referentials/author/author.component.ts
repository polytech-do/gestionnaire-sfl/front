import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { finalize } from 'rxjs/operators';
import { fadeAnimation, fadeOutAnimation, scalefadeAnimation } from 'src/app/shared/animation/animation';
import { Author } from 'src/app/shared/data/models/author.type';
import { AuthorService } from 'src/app/shared/data/services/author.service';
import { ReadRessourceComponent } from 'src/app/shared/form/crud/read.component';

@Component({
  selector: 'sfl-author',
  templateUrl: './author.component.html',
  styleUrls: ['./author.component.scss'],
  animations: [fadeAnimation, fadeOutAnimation],
})
export class AuthorComponent implements OnInit {
  authors: Author[];
  ghosts = new Array(4);

  constructor(private authorService: AuthorService) {}

  ngOnInit(): void {
    this.authorService
      .findAll()
      .pipe(finalize(() => (this.ghosts = [])))
      .subscribe((author: Author[]) => {
        this.authors = author;
      });
  }

  delete(author: Author) {
    this.authorService.delete(author).subscribe((isDeleted: boolean) => {
      if (isDeleted) {
        this.authors = this.authors.filter((a: Author) => a._id !== author._id);
      }
    });
  }
}
