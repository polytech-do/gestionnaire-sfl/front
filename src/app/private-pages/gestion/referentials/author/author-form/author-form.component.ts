import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { scalefadeAnimation } from 'src/app/shared/animation/animation';
import { Author } from 'src/app/shared/data/models/author.type';
import { AuthorService } from 'src/app/shared/data/services/author.service';
import { CrudComponent } from 'src/app/shared/form/crud/crud.component';

@Component({
  selector: 'sfl-author-form',
  templateUrl: './author-form.component.html',
  styleUrls: ['./author-form.component.scss'],
  animations: [scalefadeAnimation],
})
export class AuthorFormComponent extends CrudComponent<Author> {
  constructor(
    protected router: Router,
    protected fb: FormBuilder,
    protected authorService: AuthorService,
    protected route: ActivatedRoute
  ) {
    super(authorService, route, fb, router);
  }

  initForm(
    author: Author = {
      _id: null,
      firstname: null,
      lastName: null,
    }
  ): void {
    this.formGroup = this.fb.group({
      _id: [author?._id],
      firstname: [author?.firstname, Validators.required],
      lastName: [author?.lastName, Validators.required],
    });
  }

  submit(): void {
    super.send().subscribe((res) => this.router.navigate(['/', 'gestion', 'referentials', 'author']));
  }
}
