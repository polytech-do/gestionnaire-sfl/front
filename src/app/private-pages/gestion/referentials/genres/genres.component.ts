import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { fadeAnimation, fadeOutAnimation } from 'src/app/shared/animation/animation';
import { Genre } from 'src/app/shared/data/models/genre.type';
import { GenreService } from 'src/app/shared/data/services/genre.service';

@Component({
  selector: 'sfl-genres',
  templateUrl: './genres.component.html',
  styleUrls: ['./genres.component.scss'],
  animations: [fadeAnimation, fadeOutAnimation],
})
export class GenresComponent implements OnInit {
  genres: Genre[];
  ghosts = new Array(4);

  constructor(private genreService: GenreService) {}

  ngOnInit(): void {
    this.genreService
      .findAll()
      .pipe(finalize(() => (this.ghosts = [])))
      .subscribe((genres: Genre[]) => {
        this.genres = genres;
      });
  }

  delete(genre: Genre) {
    this.genreService.delete(genre).subscribe((isDeleted: boolean) => {
      if (isDeleted) {
        this.genres = this.genres.filter((g: Genre) => g._id !== genre._id);
      }
    });
  }
}
