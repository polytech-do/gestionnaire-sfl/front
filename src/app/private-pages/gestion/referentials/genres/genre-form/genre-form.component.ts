import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { scalefadeAnimation } from 'src/app/shared/animation/animation';
import { Genre } from 'src/app/shared/data/models/genre.type';
import { GenreService } from 'src/app/shared/data/services/genre.service';
import { CrudComponent } from 'src/app/shared/form/crud/crud.component';

@Component({
  selector: 'sfl-genre-form',
  templateUrl: './genre-form.component.html',
  styleUrls: ['./genre-form.component.scss'],
  animations: [scalefadeAnimation],
})
export class GenreFormComponent extends CrudComponent<Genre> {
  constructor(protected router: Router, protected fb: FormBuilder, protected genreService: GenreService, protected route: ActivatedRoute) {
    super(genreService, route, fb, router);
  }

  initForm(
    genre: Genre = {
      _id: null,
      label: null,
    }
  ): void {
    this.formGroup = this.fb.group({
      _id: [genre?._id],
      label: [genre?.label, Validators.required],
    });
  }

  submit(): void {
    super.send().subscribe((res) => this.router.navigate(['/', 'gestion', 'referentials', 'genre']));
  }
}
