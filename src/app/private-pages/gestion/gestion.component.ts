import { Component, OnInit } from '@angular/core';
import { fadeAnimation } from 'src/app/shared/animation/animation';
import { SideNavItem } from 'src/app/shared/component/main-layout/shared/side-nav-items';
import { AuthorService } from 'src/app/shared/data/services/author.service';
import { BookService } from 'src/app/shared/data/services/book.service';
import { FilmService } from 'src/app/shared/data/services/film.service';
import { GenreService } from 'src/app/shared/data/services/genre.service';
import { SerieService } from 'src/app/shared/data/services/serie.service';

@Component({
  selector: 'sfl-referentials',
  templateUrl: './gestion.component.html',
  styleUrls: ['./gestion.component.scss'],
  animations: [fadeAnimation],
})
export class GestionComponent implements OnInit {
  referentials = [];
  medias = [];

  constructor(
    private serieService: SerieService,
    private bookService: BookService,
    private filmService: FilmService,
    private genreService: GenreService,
    private authorService: AuthorService
  ) {}

  ngOnInit(): void {
    this.receiveData();
    this.medias = [
      {
        ressource: 'shared.resources.serie',
        routerLink: ['/', 'serie', 'edit'],
        count: 0,
      },
      {
        ressource: 'shared.resources.film',
        routerLink: ['/', 'film', 'edit'],
        count: 0,
      },
      {
        ressource: 'shared.resources.book',
        routerLink: ['/', 'book', 'edit'],
        count: 0,
      },
    ];
    this.referentials = [
      {
        ressource: 'shared.resources.genre',
        routerLink: ['/', 'gestion', 'referentials', 'genre'],
        count: 0,
      },
      {
        ressource: 'shared.resources.author',
        routerLink: ['/', 'gestion', 'referentials', 'author'],
        count: 0,
      },
    ];
  }

  receiveData(): void {
    this.serieService.count().subscribe((count: number) => (this.medias[0].count = count));
    this.filmService.count().subscribe((count: number) => (this.medias[1].count = count));
    this.bookService.count().subscribe((count: number) => (this.medias[2].count = count));

    this.genreService.count().subscribe((count: number) => (this.referentials[0].count = count));
    this.authorService.count().subscribe((count: number) => (this.referentials[1].count = count));
  }
}
