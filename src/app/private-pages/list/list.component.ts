import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal, DomPortal, TemplatePortal } from '@angular/cdk/portal';
import { AfterViewInit, Component, ComponentRef, ElementRef, OnDestroy, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { forkJoin } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { fadeAnimation, fadeOutAnimation } from 'src/app/shared/animation/animation';
import { SpotlightComponent } from 'src/app/shared/component/spotlight/spotlight.component';
import { Book } from 'src/app/shared/data/models/book.type';
import { Film } from 'src/app/shared/data/models/film.type';
import { Genre } from 'src/app/shared/data/models/genre.type';
import { Media } from 'src/app/shared/data/models/media.type';
import { Serie } from 'src/app/shared/data/models/serie.type';
import { Default } from 'src/app/shared/data/models/shared/default.type';
import { TypeEnum } from 'src/app/shared/data/models/type.type';
import { BookService } from 'src/app/shared/data/services/book.service';
import { FilmService } from 'src/app/shared/data/services/film.service';
import { GenreService } from 'src/app/shared/data/services/genre.service';
import { SerieService } from 'src/app/shared/data/services/serie.service';

@Component({
  selector: 'sfl-list-page',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  animations: [fadeAnimation, fadeOutAnimation],
})
export class ListComponent implements OnInit, AfterViewInit, OnDestroy {
  list: Media[];
  overlayRef: OverlayRef;
  spotlightRef: ComponentRef<SpotlightComponent>;
  ghosts = new Array(3);

  filterFormGroup: FormGroup;
  searchValue: string;
  types: TypeEnum[] = [];
  genres: Genre[];

  constructor(
    private serieService: SerieService,
    private filmService: FilmService,
    private bookService: BookService,
    private genreService: GenreService,
    private fb: FormBuilder,
    private overlay: Overlay
  ) {}

  ngOnInit(): void {
    this.receiveData();
    this.initFilterForm();
  }

  ngAfterViewInit() {
    this.overlayRef = this.overlay.create();
    const spotlightPortal = new ComponentPortal(SpotlightComponent);
    this.spotlightRef = this.overlayRef.attach(spotlightPortal);
    this.spotlightRef.instance.search.subscribe((search: string) => {
      this.searchValue = search;
    });
  }

  ngOnDestroy() {
    this.overlayRef.detach();
  }

  initFilterForm() {
    this.filterFormGroup = this.fb.group({
      types: [[]],
      genres: [[]],
    });
  }

  openSpotlight() {
    this.spotlightRef.instance.toggleSpotlight();
  }

  receiveData() {
    forkJoin([this.serieService.findAll(), this.filmService.findAll(), this.bookService.findAll()])
      .pipe(finalize(() => (this.ghosts = [])))
      .subscribe(([series, film, book]: [Serie[], Film[], Book[]]) => {
        this.list = [...series, ...film, ...book];
      });
    this.genreService.findAll().subscribe((genres: Genre[]) => (this.genres = genres));
    for (const key in TypeEnum) {
      if (key) {
        this.types.push(TypeEnum[key]);
      }
    }
  }

  compare(a: Default, b: Default): boolean {
    return a?._id === b?._id;
  }
}
