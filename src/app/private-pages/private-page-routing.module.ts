import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotFoundComponent } from '../public-pages/not-found/not-found.component';
import { MainLayoutComponent } from '../shared/component/main-layout/main-layout.component';
import { SecureGuard } from '../shared/guard/secure-guard.guard';
import { BookFormComponent } from './books/book-form/book-form.component';
import { BookViewComponent } from './books/book-view/book-view.component';
import { FilmFormComponent } from './films/film-form/film-form.component';
import { FilmViewComponent } from './films/film-view/film-view.component';
import { GestionComponent } from './gestion/gestion.component';
import { AuthorFormComponent } from './gestion/referentials/author/author-form/author-form.component';
import { AuthorComponent } from './gestion/referentials/author/author.component';
import { GenreFormComponent } from './gestion/referentials/genres/genre-form/genre-form.component';
import { GenresComponent } from './gestion/referentials/genres/genres.component';
import { HomeComponent } from './home/home.component';
import { ListComponent } from './list/list.component';
import { ProfileComponent } from './profile/profile.component';
import { SerieFormComponent } from './series/serie-form/serie-form.component';
import { SerieViewComponent } from './series/serie-view/serie-view.component';
import { SharedLinkFormComponent } from './shared-links/shared-link-form/shared-link-form.component';
import { SharedLinksComponent } from './shared-links/shared-links.component';

const routes: Routes = [
  {
    path: '',
    canActivate: [SecureGuard],
    component: MainLayoutComponent,
    children: [
      {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full',
      },
      {
        path: 'home',
        component: HomeComponent,
      },
      {
        path: 'list',
        component: ListComponent,
      },
      {
        path: 'profile',
        component: ProfileComponent,
      },
      {
        path: 'gestion',
        children: [
          {
            path: '',
            component: GestionComponent,
          },
          {
            path: 'referentials/genre',
            children: [
              {
                path: '',
                component: GenresComponent,
              },
              {
                path: 'edit/:id',
                component: GenreFormComponent,
              },
              {
                path: 'edit',
                component: GenreFormComponent,
              },
            ],
          },
          {
            path: 'referentials/author',
            children: [
              {
                path: '',
                component: AuthorComponent,
              },
              {
                path: 'edit/:id',
                component: AuthorFormComponent,
              },
              {
                path: 'edit',
                component: AuthorFormComponent,
              },
            ],
          },
        ],
      },
      {
        path: 'sharedLinks',
        component: SharedLinksComponent,
      },
      {
        path: 'sharedLinks/edit',
        component: SharedLinkFormComponent,
      },
      {
        path: 'serie',
        children: [
          {
            path: 'view/:id',
            component: SerieViewComponent,
          },
          {
            path: 'edit/:id',
            component: SerieFormComponent,
          },
          {
            path: 'edit',
            component: SerieFormComponent,
          },
        ],
      },
      {
        path: 'film',
        children: [
          {
            path: 'view/:id',
            component: FilmViewComponent,
          },
          {
            path: 'edit/:id',
            component: FilmFormComponent,
          },
          {
            path: 'edit',
            component: FilmFormComponent,
          },
        ],
      },
      {
        path: 'book',
        children: [
          {
            path: 'view/:id',
            component: BookViewComponent,
          },
          {
            path: 'edit/:id',
            component: BookFormComponent,
          },
          {
            path: 'edit',
            component: BookFormComponent,
          },
        ],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PrivatePageRoutingModule {}
