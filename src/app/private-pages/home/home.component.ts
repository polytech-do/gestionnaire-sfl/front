import { Component, OnInit } from '@angular/core';
import { Book } from 'src/app/shared/data/models/book.type';
import { Film } from 'src/app/shared/data/models/film.type';
import { Serie } from 'src/app/shared/data/models/serie.type';
import { BookService } from 'src/app/shared/data/services/book.service';
import { FilmService } from 'src/app/shared/data/services/film.service';
import { SerieService } from 'src/app/shared/data/services/serie.service';
import { finalize, tap, timeout } from 'rxjs/operators';
import { fadeOutAnimation } from 'src/app/shared/animation/animation';

@Component({
  selector: 'sfl-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  animations: [fadeOutAnimation],
})
export class HomeComponent implements OnInit {
  todayReleases: Serie[];

  currentSeries: Serie[];
  currentFilms: Film[];
  currentBooks: Book[];

  ghostsRelease = Array(2);
  ghostsSeries = Array(2);
  ghostsFilms = Array(2);
  ghostsBooks = Array(2);

  constructor(private serieService: SerieService, private filmService: FilmService, private bookService: BookService) {}

  ngOnInit(): void {
    this.receiveData();
  }

  receiveData() {
    this.serieService
      .findAllWithParams({
        inProgress: true,
        releaseDays: [new Date().getDay()],
        hasSeen: false,
      })
      .pipe(finalize(() => (this.ghostsRelease = [])))
      .subscribe((series: Serie[]) => {
        this.todayReleases = series;
      });

    this.serieService
      .findAllWithParams({ inProgress: true })
      .pipe(finalize(() => (this.ghostsSeries = [])))
      .subscribe((series: Serie[]) => {
        this.currentSeries = series;
      });

    this.filmService
      .findAllWithParams({ inProgress: true })
      .pipe(finalize(() => (this.ghostsFilms = [])))
      .subscribe((films: Film[]) => {
        this.currentFilms = films;
      });

    this.bookService
      .findAllWithParams({ inProgress: true })
      .pipe(finalize(() => (this.ghostsBooks = [])))
      .subscribe((books: Book[]) => {
        this.currentBooks = books;
      });
  }

  seenEpisode(serie: Serie) {
    serie.hasSeen = true;
    serie.lasEpisodeSeen = serie.lasEpisodeSeen ? serie.lasEpisodeSeen + 1 : serie.lasEpisodeSeen;
    this.serieService.update(serie).subscribe(() => {
      this.todayReleases = this.todayReleases.filter((s: Serie) => s._id !== serie._id);
    });
  }
}
