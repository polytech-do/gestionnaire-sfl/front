import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ngxCsv, Options } from 'ngx-csv';
import { map } from 'rxjs/operators';
import { fadeAnimation } from 'src/app/shared/animation/animation';
import { Author } from 'src/app/shared/data/models/author.type';
import { Genre } from 'src/app/shared/data/models/genre.type';
import { Me } from 'src/app/shared/data/models/me.type';
import { Media } from 'src/app/shared/data/models/media.type';
import { CurrentUserService } from 'src/app/shared/data/services/current-user.service';
import { LoginService } from 'src/app/shared/data/services/login.service';
import { MediaService } from 'src/app/shared/data/services/media.service';
import { UserService } from 'src/app/shared/data/services/user.service';
import { i18nEnum } from 'src/app/shared/models/i18n.enum';
import { I18nService } from 'src/app/shared/services/i18n.service';

@Component({
  selector: 'sfl-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  animations: [fadeAnimation],
})
export class ProfileComponent implements OnInit {
  me: Me;
  i18nFormControl: FormControl;

  constructor(
    private currentUserService: CurrentUserService,
    private userService: UserService,
    private loginService: LoginService,
    private router: Router,
    public translateService: TranslateService,
    private i18nService: I18nService,
    private mediaService: MediaService
  ) {
    this.i18nFormControl = new FormControl(translateService.getDefaultLang());
  }

  get i18nEnum(): typeof i18nEnum {
    return i18nEnum;
  }

  ngOnInit(): void {
    this.currentUserService.me$.subscribe((me: Me) => {
      this.me = me;
    });
  }

  changeI18n(lang: i18nEnum): void {
    this.i18nService.setLang(lang);
    this.translateService.setDefaultLang(lang);
  }

  createCsv(): void {
    this.mediaService.findAll().subscribe((medias: Media[]) => {
      // Mapping to have clean csv
      const csvMedia = medias.map((media: Media) => ({
        title: media.title || '',
        description: media.description || '',
        image: media.image || '',
        rate: media.rate || '',
        type: media.type || '',
        genres: media.genres?.map((g: Genre) => g.label)?.toString() || '',
        authors: media.authors?.map((a: Author) => `${a.firstname} ${a.lastName}`)?.toString() || '',
        love: media.love || '',
        wish: media.wish || '',
        inProgress: media.inProgress || '',
        lasEpisodeSeen: media.lasEpisodeSeen || '',
      }));

      const options = {
        fieldSeparator: ',',
        quoteStrings: '"',
        decimalseparator: '.',
        showLabels: true,
        showTitle: true,
        title: `${this.translateService.instant('csv.media.title')} - ${csvMedia.length}`,
        useBom: true,
        headers: [
          this.translateService.instant('csv.media.headers.title'),
          this.translateService.instant('csv.media.headers.description'),
          this.translateService.instant('csv.media.headers.image'),
          this.translateService.instant('csv.media.headers.rate'),
          this.translateService.instant('csv.media.headers.type'),
          this.translateService.instant('csv.media.headers.genres'),
          this.translateService.instant('csv.media.headers.authors'),
          this.translateService.instant('csv.media.headers.love'),
          this.translateService.instant('csv.media.headers.wish'),
          this.translateService.instant('csv.media.headers.inProgress'),
          this.translateService.instant('csv.media.headers.lasEpisodeSeen'),
        ],
      };
      new ngxCsv(csvMedia, `${this.me.displayName}-SFL-Medias`, options);
    });
  }

  logout(): void {
    this.loginService.logout();
  }

  deleteAccount(): void {
    this.userService.delete(this.me).subscribe((isDeleted: boolean) => {
      if (isDeleted) {
        this.currentUserService.me$.next(null);
        this.router.navigate(['/', 'login']);
      }
    });
  }
}
