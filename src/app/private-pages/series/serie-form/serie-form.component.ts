import { WeekDay } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { MediaChange, MediaObserver } from '@angular/flex-layout';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { scalefadeAnimation } from 'src/app/shared/animation/animation';
import { Serie } from 'src/app/shared/data/models/serie.type';
import { TypeEnum } from 'src/app/shared/data/models/type.type';
import { SerieService } from 'src/app/shared/data/services/serie.service';
import { CrudComponent } from 'src/app/shared/form/crud/crud.component';

@Component({
  selector: 'sfl-serie-form',
  templateUrl: './serie-form.component.html',
  styleUrls: ['./serie-form.component.scss'],
  animations: [scalefadeAnimation],
})
export class SerieFormComponent extends CrudComponent<Serie> implements OnDestroy, OnInit {
  mediaObserverSubscription$: Subscription;
  days: WeekDay[];
  isMobile = false;

  constructor(
    protected router: Router,
    protected fb: FormBuilder,
    protected serieService: SerieService,
    protected route: ActivatedRoute,
    private mediaObserver: MediaObserver
  ) {
    super(serieService, route, fb, router);
  }

  initForm(
    serie: Serie = {
      _id: null,
      title: null,
      love: false,
      wish: false,
      inProgress: false,
      type: TypeEnum.serie,
      hasSeen: false,
    }
  ): void {
    this.formGroup = this.mediaFormBuilder(serie, {
      endDate: [serie.endDate],
      startDate: [serie.startDate],
      releaseDays: [serie.releaseDays],
      hasSeen: [serie.hasSeen],
      lasEpisodeSeen: [serie.lasEpisodeSeen, [Validators.min(0)]],
    });
  }

  ngOnInit() {
    super.ngOnInit();
    this.days = [WeekDay.Monday, WeekDay.Tuesday, WeekDay.Wednesday, WeekDay.Thursday, WeekDay.Friday, WeekDay.Saturday, WeekDay.Sunday];

    this.mediaObserverSubscription$ = this.mediaObserver
      .asObservable()
      .pipe(
        filter((changes: MediaChange[]) => changes.length > 0),
        map((changes: MediaChange[]) => changes[0])
      )
      .subscribe((mediaChange: MediaChange) => {
        this.isMobile = mediaChange.mqAlias === 'xs' ? true : false;
      });
  }

  ngOnDestroy() {
    super.ngOnDestroy();
    this.mediaObserverSubscription$?.unsubscribe();
  }

  submit(): void {
    super.send().subscribe(() => this.router.navigate(['/', 'list']));
  }

  selectDay(day: WeekDay): void {
    const selectedDays = new Set(this.formGroup.get('releaseDays').value);
    if (selectedDays.has(day)) {
      selectedDays.delete(day);
    } else {
      selectedDays.add(day);
    }
    this.formGroup.get('releaseDays').setValue(Array.from(selectedDays));
  }

  isSelected(day: WeekDay): boolean {
    return (this.formGroup.get('releaseDays').value as WeekDay[])?.includes(day);
  }
}
