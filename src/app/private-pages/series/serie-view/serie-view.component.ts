import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { fadeAnimation, scalefadeAnimation } from 'src/app/shared/animation/animation';
import { Serie } from 'src/app/shared/data/models/serie.type';
import { SharedLink } from 'src/app/shared/data/models/shared-link.type';
import { SharedList } from 'src/app/shared/data/models/shared-list.type';
import { SerieService } from 'src/app/shared/data/services/serie.service';
import { SharedListService } from 'src/app/shared/data/services/shared-list.service';
import { DialogSharedListAddComponent } from 'src/app/shared/dialog/shared-list-add-dialog/shared-list-add-dialog.component';
import { ReadRessourceComponent } from 'src/app/shared/form/crud/read.component';

@Component({
  selector: 'sfl-serie-view',
  templateUrl: './serie-view.component.html',
  styleUrls: ['./serie-view.component.scss'],
  animations: [scalefadeAnimation, fadeAnimation],
})
export class SerieViewComponent extends ReadRessourceComponent<Serie> {
  constructor(
    protected serieService: SerieService,
    protected route: ActivatedRoute,
    protected router: Router,
    private dialog: MatDialog,
    private sharedListService: SharedListService
  ) {
    super(serieService, route, router);
  }

  delete() {
    this.serieService.delete(this.ressource).subscribe((isDeleted: boolean) => {
      if (isDeleted) {
        this.router.navigate(['/', 'list']);
      }
    });
  }

  openSharedListDialog(): void {
    this.dialog
      .open(DialogSharedListAddComponent)
      .afterClosed()
      .subscribe((sharedLink: SharedLink) => {
        if (sharedLink) {
          if (!sharedLink.sharedList.series) {
            sharedLink.sharedList.series = [];
          }
          if (!sharedLink.sharedList.series.find((l) => l._id === this.ressource._id)) {
            sharedLink.sharedList.series.push(this.ressource);
          }
          this.sharedListService.update(sharedLink.sharedList).subscribe();
        }
      });
  }
}
