import { NgModule } from '@angular/core';
import { PrivatePageRoutingModule } from './private-page-routing.module';
import { HomeComponent } from './home/home.component';
import { ListComponent } from './list/list.component';
import { SharedLinksComponent } from './shared-links/shared-links.component';
import { ProfileComponent } from './profile/profile.component';
import { BookFormComponent } from './books/book-form/book-form.component';
import { BookViewComponent } from './books/book-view/book-view.component';
import { FilmViewComponent } from './films/film-view/film-view.component';
import { FilmFormComponent } from './films/film-form/film-form.component';
import { SerieFormComponent } from './series/serie-form/serie-form.component';
import { SerieViewComponent } from './series/serie-view/serie-view.component';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '../shared/shared.module';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { SharedLinkFormComponent } from './shared-links/shared-link-form/shared-link-form.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatTooltipModule } from '@angular/material/tooltip';
import { GenresComponent } from './gestion/referentials/genres/genres.component';
import { GestionComponent } from './gestion/gestion.component';
import { GenreFormComponent } from './gestion/referentials/genres/genre-form/genre-form.component';
import { AuthorComponent } from './gestion/referentials/author/author.component';
import { AuthorFormComponent } from './gestion/referentials/author/author-form/author-form.component';
import { MatDividerModule } from '@angular/material/divider';
import { MatSelectModule } from '@angular/material/select';
import { MatDialogModule } from '@angular/material/dialog';
import { DialogSharedListAddComponent } from '../shared/dialog/shared-list-add-dialog/shared-list-add-dialog.component';
import { DirectiveModule } from '../shared/directive/directive.module';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { OverlayModule } from '@angular/cdk/overlay';
import { PortalModule } from '@angular/cdk/portal';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { PipeModule } from '../shared/pipe/pipe.module';
import { MatChipsModule } from '@angular/material/chips';
import { ClipboardModule } from 'ngx-clipboard';

@NgModule({
  declarations: [
    HomeComponent,
    ListComponent,
    SharedLinksComponent,
    ProfileComponent,
    BookFormComponent,
    BookViewComponent,
    FilmViewComponent,
    FilmFormComponent,
    SerieFormComponent,
    SerieViewComponent,
    GenresComponent,
    GestionComponent,
    GenreFormComponent,
    SharedLinkFormComponent,
    AuthorComponent,
    AuthorFormComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MatInputModule,
    MatFormFieldModule,
    PrivatePageRoutingModule,
    SharedModule,
    TranslateModule,
    MatIconModule,
    MatButtonModule,
    MatMenuModule,
    FlexLayoutModule,
    MatTooltipModule,
    MatDividerModule,
    MatSelectModule,
    MatDialogModule,
    DirectiveModule,
    MatDatepickerModule,
    MatNativeDateModule,
    OverlayModule,
    PortalModule,
    PipeModule,
    MatChipsModule,
    ClipboardModule,
  ],
  entryComponents: [DialogSharedListAddComponent],
  providers: [MatDatepickerModule],
})
export class PrivatePageModule {}
