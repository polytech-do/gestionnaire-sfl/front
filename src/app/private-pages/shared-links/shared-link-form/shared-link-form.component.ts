import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ngxCsv } from 'ngx-csv';
import { SharedLink } from 'src/app/shared/data/models/shared-link.type';
import { SharedList } from 'src/app/shared/data/models/shared-list.type';
import { SharedLinkService } from 'src/app/shared/data/services/shared-link.service';
import { SharedListService } from 'src/app/shared/data/services/shared-list.service';
import { CrudComponent } from 'src/app/shared/form/crud/crud.component';

@Component({
  selector: 'sfl-shared-link-form',
  templateUrl: './shared-link-form.component.html',
  styleUrls: ['./shared-link-form.component.scss'],
})
export class SharedLinkFormComponent extends CrudComponent<SharedLink> {
  constructor(
    protected router: Router,
    protected fb: FormBuilder,
    protected sharedLinkService: SharedLinkService,
    protected route: ActivatedRoute
  ) {
    super(sharedLinkService, route, fb, router);
  }

  initForm(
    sharedLink: SharedLink = {
      _id: null,
      sharedList: {
        _id: null,
        label: null,
        author: null,
      },
    }
  ): void {
    this.formGroup = this.fb.group({
      id: [sharedLink?._id],
      sharedList: this.fb.group({
        id: [sharedLink?.sharedList?._id],
        label: [sharedLink?.sharedList?.label],
      }),
    });
  }

  submit(): void {
    super.send().subscribe((res) => this.router.navigate(['/', 'sharedLinks']));
  }
}
