import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslateService } from '@ngx-translate/core';
import { ClipboardService } from 'ngx-clipboard';
import { finalize } from 'rxjs/operators';
import { fadeAnimation, fadeOutAnimation } from 'src/app/shared/animation/animation';
import { SharedLink } from 'src/app/shared/data/models/shared-link.type';
import { SharedList } from 'src/app/shared/data/models/shared-list.type';
import { SharedLinkService } from 'src/app/shared/data/services/shared-link.service';
import { SharedListService } from 'src/app/shared/data/services/shared-list.service';

@Component({
  selector: 'sfl-shared-links',
  templateUrl: './shared-links.component.html',
  styleUrls: ['./shared-links.component.scss'],
  animations: [fadeAnimation, fadeOutAnimation],
})
export class SharedLinksComponent implements OnInit {
  links: SharedLink[];
  ghosts = new Array(4);

  constructor(
    private sharedLinkService: SharedLinkService,
    private snackbar: MatSnackBar,
    private translateService: TranslateService,
    private clipboardService: ClipboardService
  ) {}

  ngOnInit(): void {
    this.sharedLinkService
      .findAll()
      .pipe(finalize(() => (this.ghosts = [])))
      .subscribe((links: SharedLink[]) => {
        this.links = links;
      });
  }

  copied(link: SharedLink): void {
    this.clipboardService.copy(`${window.location.origin}/#/sharedList/${link.sharedList._id}`);
    this.snackbar.open(
      this.translateService.instant('sharedLinks.snackbar.clipboard'),
      this.translateService.instant('shared.actions.close'),
      { duration: 3000 }
    );
  }

  delete(link: SharedLink): void {
    this.sharedLinkService.delete(link).subscribe((isDeleted: boolean) => {
      if (isDeleted) {
        this.links = this.links.filter((shrl: SharedLink) => shrl._id !== link._id);
      }
    });
  }
}
