import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { fadeAnimation, scalefadeAnimation } from 'src/app/shared/animation/animation';
import { Book } from 'src/app/shared/data/models/book.type';
import { SharedLink } from 'src/app/shared/data/models/shared-link.type';
import { SharedList } from 'src/app/shared/data/models/shared-list.type';
import { BookService } from 'src/app/shared/data/services/book.service';
import { SharedListService } from 'src/app/shared/data/services/shared-list.service';
import { DialogSharedListAddComponent } from 'src/app/shared/dialog/shared-list-add-dialog/shared-list-add-dialog.component';
import { ReadRessourceComponent } from 'src/app/shared/form/crud/read.component';

@Component({
  selector: 'sfl-book-view',
  templateUrl: './book-view.component.html',
  styleUrls: ['./book-view.component.scss'],
  animations: [scalefadeAnimation, fadeAnimation],
})
export class BookViewComponent extends ReadRessourceComponent<Book> {
  constructor(
    protected bookService: BookService,
    protected route: ActivatedRoute,
    protected router: Router,
    private dialog: MatDialog,
    private sharedListService: SharedListService
  ) {
    super(bookService, route, router);
  }

  delete() {
    this.bookService.delete(this.ressource).subscribe((isDeleted: boolean) => {
      if (isDeleted) {
        this.router.navigate(['/', 'list']);
      }
    });
  }

  openSharedListDialog(): void {
    this.dialog
      .open(DialogSharedListAddComponent)
      .afterClosed()
      .subscribe((sharedLink: SharedLink) => {
        if (sharedLink) {
          if (!sharedLink.sharedList.books) {
            sharedLink.sharedList.books = [];
          }
          if (!sharedLink.sharedList.books.find((l) => l._id === this.ressource._id)) {
            sharedLink.sharedList.books.push(this.ressource);
          }
          this.sharedListService.update(sharedLink.sharedList).subscribe();
        }
      });
  }
}
