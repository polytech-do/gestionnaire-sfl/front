import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { scalefadeAnimation } from 'src/app/shared/animation/animation';
import { Book } from 'src/app/shared/data/models/book.type';
import { TypeEnum } from 'src/app/shared/data/models/type.type';
import { BookService } from 'src/app/shared/data/services/book.service';
import { CrudComponent } from 'src/app/shared/form/crud/crud.component';

@Component({
  selector: 'sfl-book-form',
  templateUrl: './book-form.component.html',
  styleUrls: ['./book-form.component.scss'],
  animations: [scalefadeAnimation],
})
export class BookFormComponent extends CrudComponent<Book> {
  constructor(protected router: Router, protected fb: FormBuilder, protected bookService: BookService, protected route: ActivatedRoute) {
    super(bookService, route, fb, router);
  }

  initForm(
    book: Book = {
      _id: null,
      title: null,
      description: null,
      image: null,
      rate: null,
      genres: null,
      type: TypeEnum.book,
      love: false,
      wish: false,
      inProgress: false,
    }
  ): void {
    this.formGroup = this.mediaFormBuilder(book);
  }

  submit(): void {
    super.send().subscribe((res) => this.router.navigate(['/', 'list']));
  }
}
