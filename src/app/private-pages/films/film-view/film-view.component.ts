import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { fadeAnimation, scalefadeAnimation } from 'src/app/shared/animation/animation';
import { Film } from 'src/app/shared/data/models/film.type';
import { SharedLink } from 'src/app/shared/data/models/shared-link.type';
import { SharedList } from 'src/app/shared/data/models/shared-list.type';
import { FilmService } from 'src/app/shared/data/services/film.service';
import { SharedListService } from 'src/app/shared/data/services/shared-list.service';
import { DialogSharedListAddComponent } from 'src/app/shared/dialog/shared-list-add-dialog/shared-list-add-dialog.component';
import { ReadRessourceComponent } from 'src/app/shared/form/crud/read.component';

@Component({
  selector: 'sfl-film-view',
  templateUrl: './film-view.component.html',
  styleUrls: ['./film-view.component.scss'],
  animations: [scalefadeAnimation, fadeAnimation],
})
export class FilmViewComponent extends ReadRessourceComponent<Film> {
  constructor(
    protected filmService: FilmService,
    protected route: ActivatedRoute,
    protected router: Router,
    private dialog: MatDialog,
    private sharedListService: SharedListService
  ) {
    super(filmService, route, router);
  }

  delete(): void {
    this.filmService.delete(this.ressource).subscribe((isDeleted: boolean) => {
      if (isDeleted) {
        this.router.navigate(['/', 'list']);
      }
    });
  }

  openSharedListDialog(): void {
    this.dialog
      .open(DialogSharedListAddComponent)
      .afterClosed()
      .subscribe((sharedLink: SharedLink) => {
        if (sharedLink) {
          if (!sharedLink.sharedList.films) {
            sharedLink.sharedList.films = [];
          }
          if (!sharedLink.sharedList.films.find((l) => l._id === this.ressource._id)) {
            sharedLink.sharedList.films.push(this.ressource);
          }
          this.sharedListService.update(sharedLink.sharedList).subscribe();
        }
      });
  }
}
