import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { scalefadeAnimation } from 'src/app/shared/animation/animation';
import { Film } from 'src/app/shared/data/models/film.type';
import { TypeEnum } from 'src/app/shared/data/models/type.type';
import { FilmService } from 'src/app/shared/data/services/film.service';
import { CrudComponent } from 'src/app/shared/form/crud/crud.component';

@Component({
  selector: 'sfl-film-form',
  templateUrl: './film-form.component.html',
  styleUrls: ['./film-form.component.scss'],
  animations: [scalefadeAnimation],
})
export class FilmFormComponent extends CrudComponent<Film> {
  constructor(protected router: Router, protected fb: FormBuilder, protected filmService: FilmService, protected route: ActivatedRoute) {
    super(filmService, route, fb, router);
  }

  initForm(
    film: Film = {
      _id: null,
      title: null,
      description: null,
      image: null,
      rate: null,
      genres: null,
      love: false,
      type: TypeEnum.film,
      wish: false,
      inProgress: false,
    }
  ): void {
    this.formGroup = this.mediaFormBuilder(film);
  }

  submit(): void {
    super.send().subscribe((res) => this.router.navigate(['/', 'list']));
  }
}
