import { Environment } from 'src/app/shared/data/environment/environment';

export const environment: Environment = {
  origin: 'https://sfl.malo-polese.fr',
  gateway: {
    api: {
      name: 'apiSFL',
      version: 'v1',
    },
    endpoint: 'https://sfl-api.herokuapp.com',
  },
};
